package com.mmtr.Tests.Smoke;

import com.mmtr.ElementsClass.LoginAndLogoutPage;
import com.mmtr.TestNGAnnotation;
import org.testng.annotations.Test;

public class Login extends TestNGAnnotation {

    @Test(description = "Залогинится на сайт под админом и выйти")
    public void loginToSite(){

        LoginAndLogoutPage.email.setValue("admin@admin.ru");
        LoginAndLogoutPage.password.setValue("Aa123456");
        LoginAndLogoutPage.enterToSite.click();
    }
}
