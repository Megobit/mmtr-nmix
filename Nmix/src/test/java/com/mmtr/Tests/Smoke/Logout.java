package com.mmtr.Tests.Smoke;

import com.mmtr.ElementsClass.LoginAndLogoutPage;
import com.mmtr.TestNGAnnotation;
import org.testng.annotations.Test;

public class Logout extends TestNGAnnotation {

    @Test(description = "Залогинится на сайт под админом")
    public void logout() {

        LoginAndLogoutPage.email.setValue("nmadmin");
        LoginAndLogoutPage.password.setValue("nmadmin123");
        LoginAndLogoutPage.enterToSite.click();
        LoginAndLogoutPage.logout.click();
    }
}
