package com.mmtr.Tests.Fucn;

import com.codeborne.selenide.Condition;
import com.mmtr.ElementsClass.ClientInfoPage;
import com.mmtr.ElementsClass.CompanyPage;
import com.mmtr.StepsClass.ClientInfoStep;
import com.mmtr.StepsClass.Login;
import com.mmtr.TestNGAnnotation;
import org.testng.annotations.Test;


public class createCompany extends TestNGAnnotation {

    @Test(description = "Создание компании")
    public void createCompany(){

        Login.openSite("test");
        CompanyPage.createCompanyButton.click();
        ClientInfoStep.createURCompany();
        String nameComp = ClientInfoPage.shortNameCompInput.getValue();
        ClientInfoPage.addButton.click();
        CompanyPage.emplyoerInput.setValue(nameComp);
        CompanyPage.findCompButton.click();
        CompanyPage.nameComp.should(Condition.text(nameComp));
    }
}
