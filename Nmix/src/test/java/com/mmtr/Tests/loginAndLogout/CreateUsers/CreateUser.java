package com.mmtr.Tests.loginAndLogout.CreateUsers;

import com.mmtr.ElementsClass.EmployeePage;
import com.mmtr.ElementsClass.MainPage;
import com.mmtr.ElementsClass.NaimixOptions;
import com.mmtr.StepsClass.Login;

public class CreateUser {

    public void createUser(){
        Login.openSite("test");
        MainPage.option.click();
        NaimixOptions.employee.click();
        EmployeePage.addButton.click();
        EmployeePage.lastname.setValue("Автотест");
        EmployeePage.firstname.setValue("Тестовый");
        EmployeePage.position.click();
        EmployeePage.leader.click();
        EmployeePage.phone.setValue("0001234567");
        EmployeePage.email.setValue("test@te.st");
        EmployeePage.role.click();
        EmployeePage.adminNaimix.click();
        EmployeePage.password.setValue("123456Qw");
        EmployeePage.repeatPassword.setValue("123456Qw");
        EmployeePage.sumbmit.click();
    }
}
