package com.mmtr.StepsClass;

import static com.mmtr.PagesClass.InitPages.*;
import com.mmtr.TestNGAnnotation;

import static com.codeborne.selenide.Selenide.open;

public class Logout extends TestNGAnnotation {

    public void logout() {
        open("https://nm-test.mmtr.ru/login");
        loginAndLogoutPage().email.setValue("nmadmin");
        loginAndLogoutPage().password.setValue("nmadmin123");
        loginAndLogoutPage().enterToSite.click();
        loginAndLogoutPage().logout.click();
    }
}
