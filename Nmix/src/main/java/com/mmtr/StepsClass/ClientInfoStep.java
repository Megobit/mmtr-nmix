package com.mmtr.StepsClass;

import com.mmtr.ElementsClass.ClientInfoPage;

public class ClientInfoStep {

    private void constructor(){
      //  SelenideElement cata = $x("//div[@class='visible menu transition']//span[text()="+category+"]");
    }

    public static void chooseFormReg(String type){
        ClientInfoPage.formRegBuisnessSelect.click();
        switch(type) {
            case "Юридические лицо":{
                ClientInfoPage.urClient.click();
                break;
            }
            case "Индивидуальный предприниматель":{
                ClientInfoPage.ipClient.click();
                break;
            }
            case "Иностранная организация": {
                ClientInfoPage.intClient.click();
                break;
            }
        }
    }

    public static void chooseCategory(String category) {
        ClientInfoPage.categoryDropdown.click();
        switch (category) {
            case "Аренда": {
                ClientInfoPage.rentOption.click();
                break;
            }
            case "Вводим категорию": {
                ClientInfoPage.insertCatOption.click();
                break;
            }
            case "Виртуальная помощь": {
                ClientInfoPage.virtualHelpOption.click();
                break;
            }
            case "Врачебная практика": {
                ClientInfoPage.doctorOption.click();
                break;
            }
            case "Гусар": {
                ClientInfoPage.gusarOption.click();
                break;
            }
        }
    }

    public static void createURCompany(){
        ClientInfoStep.chooseFormReg("Юридические лицо");
        ClientInfoPage.officalNameCompInput.setValue("ООО'Компания'");
        ClientInfoPage.shortNameCompInput.setValue("ЮРКомпания");
        ClientInfoPage.INNInput.setValue("6379764077");
        ClientInfoPage.adressInput.setValue("г.Москва");
        ClientInfoPage.FIOContactInput.setValue("Пользователь Автотеста");
        ClientInfoPage.telNumberContactInput.click();
        ClientInfoPage.telNumberContactInput.setValue("0001112222");
        ClientInfoPage.emailContactInput.setValue("autotest@test.test");
        ClientInfoStep.chooseCategory("Аренда");
    }
}
