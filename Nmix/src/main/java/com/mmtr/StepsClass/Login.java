package com.mmtr.StepsClass;

import static com.codeborne.selenide.Selenide.open;
import static com.mmtr.PagesClass.InitPages.*;

public class Login {
    public static void openSite(String stend){
        switch (stend) {
            case "test": {
                open("https://nm-test.mmtr.ru/login");
                break;
            }
            case "pred": {
                open("https://nm-predprod.mmtr.ru/");
                break;
            }
        }
        loginAndLogoutPage().email.setValue("nmadmin");
        loginAndLogoutPage().password.setValue("nmadmin123");
        loginAndLogoutPage().enterToSite.click();
    }
}
