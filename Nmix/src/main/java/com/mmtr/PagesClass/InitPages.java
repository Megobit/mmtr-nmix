package com.mmtr.PagesClass;

import com.mmtr.ElementsClass.*;

public abstract class InitPages {

    private static ClientInfoPage clientInfoPage;
    private static CompanyPage companyPage;
    private static EmployeePage employeePage;
    private static LoginAndLogoutPage loginAndLogoutPage;
    private static MainPage mainPage;
    private static NaimixOptions naimixOptions;


    public static ClientInfoPage clientInfoPage() {return new ClientInfoPage();}
    public static CompanyPage companyPage(){return new CompanyPage();}
    public static EmployeePage employeePage(){return new EmployeePage();}
    public static LoginAndLogoutPage loginAndLogoutPage(){return new LoginAndLogoutPage();}
    public static MainPage mainPage(){return new MainPage();}
    public static NaimixOptions naimixOptions(){return new NaimixOptions();}
}
