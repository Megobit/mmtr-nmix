package com.mmtr;

import com.codeborne.selenide.Configuration;
import org.testng.annotations.*;

import static com.codeborne.selenide.Selenide.open;


public class TestNGAnnotation {

    @BeforeClass
    public void maxWindow() {
        Configuration.browserSize = "1500x1000";
    }
}
