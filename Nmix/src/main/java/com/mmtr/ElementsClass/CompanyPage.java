package com.mmtr.ElementsClass;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class CompanyPage {

    public static SelenideElement createCompanyButton = $x("//button//span[text() = 'Создать компанию']");
    public static SelenideElement emplyoerInput = $x("//input[@name='nameSubstringFilter']");
    public static SelenideElement findCompButton = $x("//span[@class='filter-buttons__button-send-text']");
    public static SelenideElement nameComp = $x("(//td//a)[1]");
}
