package com.mmtr.ElementsClass;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class ClientInfoPage {

    public static SelenideElement formRegBuisnessSelect = $x("//div[@name='clientType']");
    public static SelenideElement urClient = $x("//div[@role='option']//span[text()='Юридическое лицо']");
    public static SelenideElement ipClient = $x("//div[@role='option']//span[text()='Индивидуальный предприниматель']");
    public static SelenideElement intClient = $x("//div[@role='option']//span[text()='Иностранная организация']");

    // для ЮЛ
    public static SelenideElement officalNameCompInput = $x("//div[text()='Официальное название компании *']/..//input");
    public static SelenideElement shortNameCompInput = $x("//div[text()='Сокращенное название компании *']/..//input");
    public static SelenideElement INNInput = $x("//div[text()='ИНН *']/..//input");
    public static SelenideElement adressInput = $x("//div[text()='Фактический адрес *']/..//input");
    public static SelenideElement FIOContactInput = $x("//div[text()='ФИО контактного лица']/..//input");
    public static SelenideElement telNumberContactInput = $x("//div[text()='Телефон контактного лица']/..//input");
    public static SelenideElement emailContactInput = $x("//div[text()='E-mail контактного лица']/..//input");

    //общее
    public static SelenideElement promoCodeInput = $x("//div[text()='Промо-код']/..//input");
    public static SelenideElement categoryDropdown = $x("//div[text()='Категория *']/..//div[@name='categoryId']");
    public static SelenideElement addButton = $x("//button[@type='submit']");
    //категории
    public static SelenideElement rentOption = $x("//span[text()='Аренда']");
    public static SelenideElement insertCatOption = $x("//span[text()='Вводим категорию']");
    public static SelenideElement virtualHelpOption = $x("//span[text()='Виртуальная помощь']");
    public static SelenideElement doctorOption = $x("//span[text()='Врачебная практика']");
    public static SelenideElement gusarOption = $x("//span[text()='Гусар']");

}
