package com.mmtr.ElementsClass;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class EmployeePage {

    static public SelenideElement addButton = $x("//span[text()='Добавить сотрудника']");
    static public SelenideElement sumbmit = $x("//button[text()='Добавить']");

    static public SelenideElement lastname = $x("//input[@name='lastName']");
    static public SelenideElement firstname = $x("//input[@name='firstName']");
    static public SelenideElement patronymic = $x("//input[@name='patronymic']");

    static public SelenideElement position = $x("//div[@name='position']");
    static public SelenideElement chiefAccountant = $x("//span[text()='Главный бухгалтер']");
    static public SelenideElement leader = $x("//span[text()='Руководитель']");
    static public SelenideElement manager = $x("//span[text()='Менеджер']");

    static public SelenideElement phone = $x("//input[@name='phone']");
    static public SelenideElement email = $x("//input[@name='email']");

    static public SelenideElement role = $x("//div[@name='role']");
    static public SelenideElement adminNaimix = $x("//span[text()='Администратор Наймикс']");
    static public SelenideElement managerNaimix = $x("//span[text()='Менеджер Наймикс']");
    static public SelenideElement coordinatorNaimix = $x("//span[text()='Координатор Наймикс']");
    static public SelenideElement consultantNaimix = $x("//span[text()='Консультант Наймикс']");
    static public SelenideElement SB = $x("//span[text()='СБ']");

    static public SelenideElement password = $x("//input[@name='password']");
    static public SelenideElement repeatPassword = $x("//input[@name='repeatPassword']");
}
