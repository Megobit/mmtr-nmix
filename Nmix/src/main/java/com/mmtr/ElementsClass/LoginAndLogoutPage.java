package com.mmtr.ElementsClass;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class LoginAndLogoutPage {

    public static SelenideElement email = $x("//input[@name='login']");
    public static SelenideElement password = $x("//input[@name='password']");
    public static SelenideElement enterToSite = $x("//button[text()='Войти']");
    public static SelenideElement logout = $(".nmx-menu__exit");
}
